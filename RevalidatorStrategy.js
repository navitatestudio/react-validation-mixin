var Revalidator = require('revalidator');
var union = require('lodash.union');

var RevalidatorValidationStrategy = {
    validate: function(revalidatorSchema, data, key) {
        revalidatorSchema = revalidatorSchema || {};
        data = data || {};

        var errors = this._format(Revalidator.validate(data, revalidatorSchema));
        if (key === undefined) {
            union(Object.keys(revalidatorSchema), Object.keys(data)).forEach(function(error) {
                errors[error] = errors[error] || [];
            });
            return errors;
        } else {
            var result = {};
            result[key] = errors[key];
            return result;
        }
    },

    _format: function(revalidatorResult) {
        if (revalidatorResult.error !== null) {
            return revalidatorResult.errors.reduce(function(memo, detail) {
                if (!Array.isArray(memo[detail.property])) {
                    memo[detail.property] = [];
                }
                memo[detail.property].push(detail.message);
                return memo;
            }, {});
        } else {
            return {};
        }
    }

};

module.exports = RevalidatorValidationStrategy;
